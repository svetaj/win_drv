/*--------------------------------------------------------------*/
/* BIBLIOTEKA za upravljanje stampacem Mannesman Tally MT60     */
/*                                                              */
/*--------------------------------------------------------------*/

#include <bios.h>
#include <dos.h>
#include <stdio.h>
#include <conio.h>
#include <fcntl.h>
#include <direct.h>
#include <errno.h>
#include <stdlib.h>
#include <io.h>
#include <sys\types.h>
#include <sys\stat.h>
#include <string.h>
#include <memory.h>
#include "\pb\base\perif\libr\c_prt.h"
#include "\pb\base\perif\libr\c_tub.h"
#include "\pb\base\perif\libr\tub_ext.h"

#define MAXBUF 2048

int device, n, i, noread;
unsigned char buffer [MAXBUF];

int port_open = 0;   /* flag da li je port otvoren; 0 - ne */
int s_param = 0;     /* desno radno mesto s=0  */
int t_param = 2;     /* nacin uvlacenja   t=2  */

int mode = 0;                           /* promenljive potrebne za */
char mask_set[]    = "00000000\0";
char mask_reset1[] = "00000000\0";      /* TUB_read i TUB_write    */

int reply;

#define TIME_OUT      300               /* Time out za TUB_read */
#define TIME_OUT_INF   -1               /* Time out za TUB_read */
                                        /* kod conn_aff         */
extern int debug;

/*--------------------------------------------------------------*/
/* konektovanje na PRT servis                                   */
/*--------------------------------------------------------------*/

int PRT_connect (char *inst)
{
     fprintf(stderr, "PRT_connect: ne radi nista\n");
     return(0);
}


/*--------------------------------------------------------------*/
/* komanda PRT_conn_aff                                         */
/*--------------------------------------------------------------*/

int PRT_conn_aff (char *inst, char type_com, int left_right, char *dummy)
{
     char str_par[51];
     unsigned char tab_int_crt[156];

     int modech;
     char mask_reset[9], mask_wires[9];
     char *tto;
     int *num_crt, *num_phrases;   /* broj procitanih karaktera sa porta */
                                   /* broj fraza */
     int *llread;

     reply = TUB_connect (inst);
     fprintf (stderr, "PRT_conn_aff: TUB_connect: %d\n", reply);
     if (reply != 0)
        return (reply);

     if (left_right == 2)
        s_param = 0;
     else
        s_param = 1;

     strcpy (str_par, "VEL=4800,8,E,1 OPT=100 LES=3942 XON=3131 XOF=3133");

     memset (tab_int_crt,0,156);   /* tabela za kontrolu citanja recenice */

     tab_int_crt[155] = '\x10';     /* 9b - pocetak - escape               */
     tab_int_crt[10]  = '\x50';     /* LF - kraj recenice                  */
     tab_int_crt[17]  = '\x30';     /* XOFF                                */
     tab_int_crt[19]  = '\x20';     /* XON                                 */
     tab_int_crt[0]   = '\x40';     /* Ignore  NULL                        */

     reply = TUB_open (inst, type_com, str_par, MAXBUF,
                       156, tab_int_crt, 512, MAXBUF-512);
     fprintf(stderr, "PRT_conn_aff: TUB_open: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     port_open = 1;

     modech = 1;
     strcpy(mask_reset,"00001111");
     tto[0]='\0';
     reply = TUB_change_mode (inst, type_com, modech, mask_reset,
                               tto, num_crt, num_phrases, mask_wires);
     fprintf(stderr, "PRT_conn_aff: TUB_change_mode: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));

/* select document printer , reset  */

     buffer[0] = '\x9b'; /* CSI */
     buffer[1] = '\x34'; /* 4   */
     buffer[2] = '\x69'; /* i   */
     buffer[3] = '\x1b'; /* ESC */
     buffer[4] = '\x63'; /* c   */
     buffer[5] = '\x95'; /* MW  */

     reply = TUB_write (inst, type_com, mode, 6,
                               buffer, mask_set, mask_reset1);
     fprintf(stderr, "PRT_conn_aff: TUB_write: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));

     reply = TUB_read (inst,type_com,TIME_OUT,MAXBUF,buffer,llread);
     any();
     fprintf(stderr, "PRT_conn_aff: TUB_read: %d\n", reply);
     fprintf(stderr, "PRT_conn_aff: TUB_read: %d\n", *llread);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     any();
     println_hex ("reset", buffer, *llread);

     buffer[0] = '\x9b';
     if (s_param == 1)
            buffer[1] = '\x31'; /* levi       */
     else
            buffer[1] = '\x30'; /* desni      */
     buffer[2] = '\x3b';
     fprintf (stderr, "nacin uvlacenja %d \n", t_param);
     switch (t_param) {
        case 0:
           buffer[3] = '\x30';       /* nacin uvlacenja */
           break;
        case 1:
           buffer[3] = '\x31';
           break;
        case 2:
           buffer[3] = '\x32';
           break;
        case 3:
           buffer[3] = '\x33';
           break;
        default:
           buffer[3] = '\x30';
      }
     buffer[4] = '\x3b';
     buffer[5] = '\x30';
     buffer[6] = '\x30';
     buffer[7] = '\x35';     /* ceka 5x200 ms */
     buffer[8] = '\x2e';
     buffer[9] = '\x78';
     buffer[10] = '\x95';     /* MW  */

     reply = TUB_write (inst,type_com,mode,11,
                               buffer,mask_set,mask_reset1);
     fprintf(stderr, "PRT_conn_aff: TUB_write: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     any();

     reply = TUB_read (inst,type_com,TIME_OUT,MAXBUF,buffer,llread);
     fprintf(stderr, "PRT_conn_aff: TUB_read: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     any();
     println_hex ("feed", buffer, *llread);

/* special vertical position, line 1 with automatic width recognition */

     buffer[0] = '\x9b';     /* CSI */
     buffer[1] = '\x31';     /* 1   */
     buffer[2] = '\x34';     /* 4   */
     buffer[3] = '\x2e';     /* .   */
     buffer[4] = '\x79';     /* y   */
     buffer[5] = '\x95';     /* MW  */

     reply = TUB_write (inst, type_com, mode, 6, buffer,
                         mask_set, mask_reset1);
     fprintf(stderr, "PRT_conn_aff: TUB_write: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     any();

     reply = TUB_read (inst,type_com,TIME_OUT_INF,MAXBUF,buffer,llread);
     fprintf(stderr, "PRT_conn_aff: TUB_read: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     any();
     println_hex ("line 1", buffer, *llread);

     if (s_param == 0)
          fprintf(stderr, "upaljena desna lampica\n");
     else
          fprintf(stderr, "upaljena leva lampica\n");

/* set reference edges */

     buffer[0] = '\x9b';     /* CSI */
     buffer[1] = '\x32';     /* 2   */
     buffer[2] = '\x2d';     /*     */
     buffer[3] = '\x73';     /*     */
     buffer[4] = '\x95';     /* MW  */

     reply = TUB_write (inst, type_com, mode, 5, buffer,
                         mask_set, mask_reset1);
     fprintf(stderr, "PRT_conn_aff: TUB_write: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     any();

     reply = TUB_read (inst,type_com,TIME_OUT_INF,MAXBUF,buffer,llread);
     fprintf(stderr, "PRT_conn_aff: TUB_read: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     any();
     println_hex ("set ref. edges", buffer, *llread);

/* request current form and line number */

     buffer[0] = '\x9b';     /* CSI */
     buffer[1] = '\x33';     /* 3   */
     buffer[2] = '\x2e';     /* .   */
     buffer[3] = '\x7d';     /* }   */

     reply = TUB_write (inst, type_com, mode, 4, buffer,
                         mask_set, mask_reset1);
     fprintf(stderr, "PRT_conn_aff: TUB_write: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     any();

     reply = TUB_read (inst,type_com,TIME_OUT_INF,MAXBUF,buffer,llread);
     fprintf(stderr, "PRT_conn_aff: TUB_read: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     any();
     println_hex ("form & line number", buffer, *llread);

/* Upper edge setting */

     buffer[0] = '\x9b';     /* CSI */
     buffer[1] = '\x31';     /* 1   */      /* = 10 * 1/120 inch */
     buffer[2] = '\x30';     /* 0   */
     buffer[3] = '\x2e';     /* .   */
     buffer[4] = '\x73';     /* s   */
     buffer[5] = '\x95';     /* MW  */

     reply = TUB_write (inst, type_com, mode, 6, buffer,
                         mask_set, mask_reset1);
     fprintf(stderr, "PRT_conn_aff: TUB_write: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     any();

     reply = TUB_read (inst,type_com,TIME_OUT_INF,MAXBUF,buffer,llread);
     fprintf(stderr, "PRT_conn_aff: TUB_read: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     any();
     println_hex ("Upper edge setting", buffer, *llread);

/* Horizontal edge setting */

     buffer[0] = '\x9b';     /* CSI */
     buffer[1] = '\x31';     /* 1   */	    /* = 18 * 1/120 inch */
     buffer[2] = '\x38';     /* 8   */
     buffer[3] = '\x3b';     /* ;   */
     buffer[4] = '\x2e';     /* .   */
     buffer[5] = '\x75';     /* u   */
     buffer[6] = '\x95';     /* MW  */

     reply = TUB_write (inst, type_com, mode, 7, buffer,
                         mask_set, mask_reset1);
     fprintf(stderr, "PRT_conn_aff: TUB_write: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     any();

     reply = TUB_read (inst,type_com,TIME_OUT_INF,MAXBUF,buffer,llread);
     fprintf(stderr, "PRT_conn_aff: TUB_read: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     any();
     println_hex ("Horizontal edge setting", buffer, *llread);

     return (PRT_OKAY);
}


/*--------------------------------------------------------------*/
/* komanda PRT_print                                            */
/*--------------------------------------------------------------*/

int PRT_print (char *inst, char type_com, int controller,
               char *instring, int lbuffer)
{
     int *llread;

     fprintf(stderr, "inst=%s type_com=%c controller=%d lbuffer=%d\n",
                     inst, type_com, controller, lbuffer);
     any();
     if( port_open != 1)
         return(PRT_SPEC_ERROR);

     memset ((void *)buffer, 0, (size_t) MAXBUF);
     memcpy ((void *)buffer, (void *) instring, (size_t) lbuffer);
     buffer[lbuffer] = '\x95';    /* MW */

     fprintf(stderr, "PRT_print: TUB_write: -->%s<--\n", buffer);
     reply = TUB_write (inst, type_com, mode, lbuffer+1, buffer,
                         mask_set, mask_reset1);
     fprintf(stderr, "PRT_print: TUB_write: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     any();

     reply = TUB_read (inst, type_com, TIME_OUT, MAXBUF, buffer, llread);
     fprintf(stderr, "PRT_print: TUB_read: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     any();
     fprintf(stderr, "PRT_print: TUB_read: *llread=%d\n", *llread);
     any();
     println_hex ("text", buffer, 8);
/*     println_hex ("text", buffer, *llread);           */
     any();

     return(PRT_OKAY);
}


/*--------------------------------------------------------------*/
/* komanda PRT_eject                                            */
/*--------------------------------------------------------------*/

int PRT_eject(char *inst, char type_com)
{
     int *llread;

     fprintf (stderr, "PRT_eject: inst -->%s<-- type_com -->%c<--\n",
			     inst, type_com);
     any();
     if( port_open != 1)
         return(PRT_SPEC_ERROR);

     buffer[0] = '\x9b';     /* CSI */
     buffer[1] = '\x38';     /* 8   */
     buffer[2] = '\x2e';     /* .   */
     buffer[3] = '\x79';     /* y   */
     buffer[4] = '\x95';     /* MW  */

     reply = TUB_write (inst, type_com, mode, 5, buffer,
                         mask_set, mask_reset1);
     fprintf(stderr, "PRT_eject: TUB_write: %d\n", reply);
     if (reply != TUB_OKAY)
         return(TUB_to_PRT(reply));
     any();

     reply = TUB_read (inst, type_com, TIME_OUT, MAXBUF, buffer, llread);
     fprintf(stderr, "PRT_eject: TUB_read: %d\n", reply);
     any();
     println_hex ("eject", buffer, *llread);

     return(TUB_to_PRT(reply));
}

/*--------------------------------------------------------------*/
/* komanda PRT_identify                                         */
/*--------------------------------------------------------------*/

int PRT_identify (char *inst, char type_com, char *id)
{
    id[0] = '\x2f';
    id[1] = '\x43';
    id[2] = '\x41';     /* ovo je ID koji odgovara PR50 */
    id[3] = '\x30';
    id[4] = '\x20';
    fprintf(stderr, "PRT_identify: PR50\n");
    return(PRT_OKAY);
}


/*--------------------------------------------------------------*/
/* komanda PRT_disc_pr                                          */
/*--------------------------------------------------------------*/

int PRT_disc_pr(char *inst, char type_com)
{
     reply = TUB_close (inst, type_com);
     fprintf(stderr, "PRT_disc_pr: TUB_close: %d\n", reply);
     port_open = 0;
     if (reply != TUB_OKAY)
          return(TUB_to_PRT(reply));
     any();

     reply = TUB_disconnect (inst);
     fprintf(stderr, "PRT_disc_pr: TUB_disconnect: %d\n", reply);
     return(PRT_OKAY);
}


/*--------------------------------------------------------------*/
/* komanda PRT_turn_page                                        */
/*--------------------------------------------------------------*/

int PRT_turn_page(char *inst, char type_com)
{
     fprintf(stderr, "PRT_turn_page: ne radi nista\n");
     return(PRT_OKAY);
}


/*--------------------------------------------------------------*/
/* diskonekt od PRT servisa                                     */
/*--------------------------------------------------------------*/

int PRT_disconnect(char *inst)
{
     fprintf(stderr, "PRT_disconnect: ne radi nista\n");
     return(0);
}



/*--------------------------------------------------------------*/
/* UTILITY: stampa buffer u hex formatu                         */
/*--------------------------------------------------------------*/

println_hex (const char *text, unsigned char *answ, int max)
{
     int i;
/*
     if (max > 0)
         fprintf(stderr, "%s\t: hex value : |", text);

     for (i=0; i < max; i++)
         fprintf(stderr, " %x |", answ[i] );

     if (max > 0)
         fprintf(stderr, "\n");

     any();
*/
}

/*--------------------------------------------------------------*/
/* UTILITY: konverzija TUB errors u PRT errors                  */
/*--------------------------------------------------------------*/
int TUB_to_PRT ( int tub_err)
{

      switch (tub_err)
      {


          case      TUB_OKAY:
                    return (PRT_OKAY);

/*
                    +-----------+
                    |   ABORT   |
                    +-----------+
*/

          case      TUB_OUT_OF_LINE:
                    return (PRT_OFF);

          case      TUB_LINE_ERROR:
          case      TUB_CHANNEL_ERROR:
          case      TUB_DTR_OFF:
                    return (PRT_LINE_ERROR);

          case      TUB_COMMAND_ERROR:
                    return (PRT_COMMAND_ERROR);

          case      TUB_NO_FREE:
                    return (PRT_NO_FREE);

          case      TUB_SYSTEM_ERROR:
                    return (PRT_REQ_CONF_ERROR);

          case      TUB_TOO_MANY_OUTPUTS:
                    return (PRT_SPEC_ERROR);

          case      TUB_BUFFER_OVERFLOW:
                    return (PRT_LINE_ERROR);

          case      TUB_TX_PENDING:
                    return (PRT_TRASMISSION_ERROR);

          case      TUB_NO_BUFFER:
                    return (PRT_LINE_ERROR);

/*
                    +-----------+
                    |  WARNING  |
                    +-----------+
*/

          case      TUB_NO_PHRASES:
          case      TUB_END_BY_UC:
                    return (PRT_COMMAND_INCOMPLETE);

          case      TUB_OFF_CONNECT:
                    return (PRT_OFF);

          case      TUB_END_BY_TIMEOUT:
                    return (PRT_LINE_ERROR);
          default :
                    return (tub_err);
          }
}


extern char com_mod;
extern char *param;
extern int controller;


any()
{
    if (debug == 2) {
       fprintf(stderr,
       "port_open -->%d<-- com_mod -->%c<-- controller -->%d<-- param -->%s<--\n",
		  port_open, com_mod, controller, param);
       printf ("press any key to continue ...\n");
	getch();
    }
    else if (debug == 3)
	flushall ();
}
